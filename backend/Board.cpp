#include "Board.hpp"

#include <cassert>
#include <string>

Board::Board(std::string const& starting_grid) : _turn(Team::White) {
    for (size_t i = 0; i < this->_grid.size(); i++)
        this->_grid[i] = Piece::from_char(starting_grid[i]);
}

ResponseCode Board::play_move(Move const move) {
    if (!move.in_grid())
        return ResponseCode::InvalidPosition;

    if (move.from == move.to)
        return ResponseCode::OriginIsDest;

    Piece& from_piece = this->_grid[move.from.as_index()];
    if (from_piece.team != this->_turn)
        return ResponseCode::NoPieceInOrigin;

    Team const team = from_piece.team;

    Piece& to_piece = this->_grid[move.to.as_index()];
    if (team == to_piece.team)
        return ResponseCode::OwnPieceInDest;

    // NOTE: doesn't check if the move will cause a check
    if (!from_piece.can_move(move, *this))
        return ResponseCode::IllegalMove;

    // create copies in case there will be a need to rewind
    // sizeof(Piece) == 8, so it's cheap to copy
    Piece const to_piece_copy = to_piece;
    Piece const from_piece_copy = from_piece;
    to_piece = from_piece;
    from_piece = Piece();
    if (this->in_check(team)) {
        // rewind the step
        from_piece = from_piece_copy;
        to_piece = to_piece_copy;
        return ResponseCode::WillEnterCheck;
    }

    this->_turn = enemy_team(this->_turn);

    if (this->in_check(enemy_team(team)))
        return ResponseCode::Check;

    return ResponseCode::Success;
}

// displays the board
std::string Board::display() const {
    std::string output = "";

    for (auto const& piece : this->_grid)
        output += piece.display();

    output += std::to_string((int) this->_turn);
    return output;
}

// this function uses a really naive, stupid and slow searching methods, but idgaf.
// if it works then it works
bool Board::in_check(Team const team) const {
    // find team's king
    size_t king_index = 0;
    for (; king_index < this->_grid.size(); king_index++) {
        Piece const& piece = this->_grid[king_index];
        if (piece.kind == PieceKind::King && piece.team == team)
            break;
    }

    // if the king is not in the grid, then it's already a "mate"
    if (king_index >= _grid.size())
        return true;

    Team const other_team = enemy_team(team);

    // go over all of the enemy pieces and check if one of their possible moves threatens the king
    for (size_t i = 0; i < this->_grid.size(); i++) {
        Piece const& piece = this->_grid[i];
        if (piece.team != other_team) continue;

        std::vector<Position> const dests = piece.generate_dests(Position::from_index(i), *this);
        for (auto const& dest : dests) {
            if (dest.as_index() == king_index)
                return true;
        }
    }

    return false;
}

Piece& Board::operator[](Position const pos) {
    return this->_grid[pos.as_index()];
}

Piece const& Board::operator[](Position const pos) const {
    return this->_grid[pos.as_index()];
}
