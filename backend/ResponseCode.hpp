#pragma once

enum ResponseCode {
    Success = 0,
    Check = 1,
    NoPieceInOrigin = 2,
    OwnPieceInDest = 3,
    WillEnterCheck = 4,
    InvalidPosition = 5,
    IllegalMove = 6,
    OriginIsDest = 7,
    Mate = 8,
};
