#include "Piece.hpp"
#include "Board.hpp"

#include <cassert>

Piece::Piece(PieceKind const kind, Team const team) : kind(kind), team(team) { }

std::string Piece::display() const {
    char c;
    switch (this->kind) {
    case PieceKind::None:   c = '#'; break;
    case PieceKind::Pawn:   c = 'p'; break;
    case PieceKind::Knight: c = 'n'; break;
    case PieceKind::Bishop: c = 'b'; break;
    case PieceKind::Rook:   c = 'r'; break;
    case PieceKind::Queen:  c = 'q'; break;
    case PieceKind::King:   c = 'k'; break;
    default: assert(false && "unreachable");
    }

    // lowercase == White
    // uppercase == Black
    if (this->team == Team::Black)
        c = std::toupper(c);

    return {c};
}

Piece Piece::from_char(char c) {
    Team const team = std::isupper(c) ? Team::Black : Team::White;
    switch (std::tolower(c)) {
    case '#': return Piece(PieceKind::None, Team::None);
    case 'p': return Piece(PieceKind::Pawn, team);
    case 'n': return Piece(PieceKind::Knight, team);
    case 'b': return Piece(PieceKind::Bishop, team);
    case 'r': return Piece(PieceKind::Rook, team);
    case 'q': return Piece(PieceKind::Queen, team);
    case 'k': return Piece(PieceKind::King, team);
    }

    return Piece(); // should never reach here
}

// another naive, stupid and slow implementation, but if it works it works so idc
bool Piece::can_move(Move const move, Board const& board) const {
    std::vector<Position> const dests = this->generate_dests(move.from, board);

    for (auto const& dest : dests)
        if (dest == move.to)
            return true;

    return false;
}

static std::vector<Position> pawn_dests(Team const team, Position const origin, Board const& board) {
    int8_t const direction = team == Team::White ? 1 : -1;
    int8_t const start_row = team == Team::White ? 2 : 7;

    std::vector<Position> dests = {};
    Position pos = origin;

    // one step forward
    pos = origin + Position(direction, 0);
    if (pos.in_grid() && board[pos].team == Team::None) dests.push_back(pos);

    // Yemen step
    if (origin.row == start_row) {
        pos = origin + Position(2 * direction, 0);
        // no need to check if it's in the grid bounds, because it's on the start row
        if (board[pos].team == Team::None)
            dests.push_back(pos);
    }

    // eat an enemy piece diagonally
    pos = origin + Position(direction, -1);
    if (pos.in_grid() && board[pos].team == enemy_team(team))
        dests.push_back(pos);

    pos = origin + Position(direction, 1);
    if (pos.in_grid() && board[pos].team == enemy_team(team))
        dests.push_back(pos);

    return dests;
}

static std::vector<Position> knight_dests(Team const team, Position const origin, Board const& board) {
    static const Position offsets[] = {
        Position(-1, -2),
        Position(1, -2),
        Position(-2, -1),
        Position(-2, 1),
        Position(-1, 2),
        Position(1, 2),
        Position(2, -1),
        Position(2, 1),
    };

    std::vector<Position> dests = {};

    for (Position const offset : offsets) {
        Position const pos = origin + offset;
        if (!pos.in_grid()) continue;
        Piece const& piece = board[pos];
        if (piece.team == team) continue;
        dests.push_back(pos);
    }

    return dests;
}

static std::vector<Position> sliding_piece(
    std::vector<Position> const& offsets,
    Team const team,
    Position const origin,
    Board const& board
) {
    std::vector<Position> dests = {};

    for (Position const offset : offsets) {
        Position pos = origin;
        while ((pos += offset).in_grid()) {
            Piece const& piece = board[pos];
            if (piece.team != team) dests.push_back(pos);
            if (piece.team != Team::None) break;
        }
    }

    return dests;
}

static std::vector<Position> bishop_dests(Team const team, Position const origin, Board const& board) {
    return sliding_piece({
        Position(1, 1),
        Position(1, -1),
        Position(-1, -1),
        Position(-1, 1),
    }, team, origin, board);
}

static std::vector<Position> rook_dests(Team const team, Position const origin, Board const& board) {
    return sliding_piece({
        Position(0, 1),
        Position(0, -1),
        Position(1, 0),
        Position(-1, 0),
    }, team, origin, board);
}

static std::vector<Position> queen_dests(Team const team, Position const origin, Board const& board) {
    std::vector<Position> dests = rook_dests(team, origin, board);
    std::vector<Position> dests_bishop = bishop_dests(team, origin, board);
    dests.insert(dests.end(), dests_bishop.begin(), dests_bishop.end());
    return dests;
}

static std::vector<Position> king_dests(Team const team, Position const origin, Board const& board) {
    std::vector<Position> dests = {};

    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if (i == 0 && j == 0) continue;
            Position const pos = origin + Position(i, j);
            if (!pos.in_grid()) continue;
            Piece const& piece = board[pos];
            if (piece.team != team) dests.push_back(pos);
        }
    }

    return dests;
}

std::vector<Position> Piece::generate_dests(Position const origin, Board const& board) const {
    // fuck. it gives me a warning about this being a c99 extension
    // I fucking hate "modern" cpp
    /*
    typedef std::function<std::vector<Position>(Team const team, Position const origin, Board const& board)> gen_func_t;
    static const gen_func_t functions[(int) PieceKind::COUNT] = {
        [(int) PieceKind::Pawn]   = pawn_dests,
        [(int) PieceKind::Knight] = knight_dests,
        [(int) PieceKind::Bishop] = bishop_dests,
        [(int) PieceKind::Rook]   = rook_dests,
        [(int) PieceKind::Queen]  = queen_dests,
        [(int) PieceKind::King]   = king_dests,
    };

    if (this->kind == PieceKind::None) return {};

    return functions[(int) this->kind](this->team, origin, board);
    */

    // THE POWER OF COPY & PASTE IN ACTION
    // COPY & PASTE >>>>>>>>> shitty "polymorphism" 🤓
    switch (this->kind) {
    case PieceKind::None:   return {};
    case PieceKind::Pawn:   return pawn_dests(this->team, origin, board);
    case PieceKind::Knight: return knight_dests(this->team, origin, board);
    case PieceKind::Bishop: return bishop_dests(this->team, origin, board);
    case PieceKind::Rook:   return rook_dests(this->team, origin, board);
    case PieceKind::Queen:  return queen_dests(this->team, origin, board);
    case PieceKind::King:   return king_dests(this->team, origin, board);
    default: assert(false && "unreachable");
    }
}

Team enemy_team(Team const color) {
    switch (color) {
    case Team::White: return Team::Black;
    case Team::Black: return Team::White;
    default: assert(false && "unreachable");
    }
}
