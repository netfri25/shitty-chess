#pragma once

#include "Move.hpp"
#include "Piece.hpp"
#include "ResponseCode.hpp"

#include <array>

class Board {
public:
    Board(std::string const& starting_grid = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR");

    // plays the given move on the grid (if possible)
    // returns a response code that represents the validity of the move
    ResponseCode play_move(Move const move);

    // displays the board
    std::string display() const;

    // returns true if the team is in check
    bool in_check(Team const team) const;

    Piece& operator[](Position const pos);
    Piece const& operator[](Position const pos) const;

private:
    // zero initialize all fields
    std::array<Piece, 64> _grid;
    Team _turn;
};
