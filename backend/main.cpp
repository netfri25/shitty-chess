#include <iostream>

#include "Board.hpp"
#include "Move.hpp"

int main() {
    Board board = {};
    std::string const display = board.display();
    char const* const c_str = display.c_str();
    std::cout << c_str << std::endl;
    std::cout << display.length() << std::endl;

    Position const from = Position(2, 3);
    Position const to = Position(4, 3);
    Move const move = Move(from, to);

    std::cout << (int) board[from].kind << std::endl;
    std::cout << (int) board[to].kind << std::endl;
    std::cout << board.play_move(move) << std::endl;
}
