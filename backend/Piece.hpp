#pragma once

#include "Move.hpp"

#include <string>
#include <vector>

class Board; // forward declaration

// the kind of the piece
enum class PieceKind {
    None = -1,
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
};

// the team of the piece
enum class Team {
    None = -1,
    White = 0,
    Black = 1,
};

// get the enemy's team
Team enemy_team(Team const color);

// public on purpose (that's why I'm using struct over a class)
struct Piece {
    PieceKind kind;
    Team team;

    Piece(PieceKind const kind = PieceKind::None, Team const team = Team::None);

    // convert the character to a piece
    static Piece from_char(char c);

    // tells if the piece can play this move or not
    bool can_move(Move const move, Board const& board) const;

    // generates all of the possible destinations that this piece can reach
    std::vector<Position> generate_dests(Position const origin, Board const& board) const;

    // displays the Piece
    std::string display() const;
};
