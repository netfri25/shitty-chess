#pragma once

#include <cstddef>
#include <cstdint>
#include <string>

struct Position {
public:
    int8_t row = 0;
    int8_t col = 0;

    Position(int8_t const row, int8_t const col);

    // will return 0, 0 if the position in invalid
    static Position from_string(std::string const& input);

    // returns true if `row` and `col` are inside the grid
    bool in_grid() const;

    // displays the Position
    std::string display() const;

    // returns the position as an index in the grid
    size_t as_index() const;

    // the position from an index in the grid
    static Position from_index(size_t const index);

    bool operator==(Position const& other) const;

    Position& operator+=(Position const& other);
    Position operator+(Position const& other) const;
};

struct Move {
    Position from;
    Position to;

    Move(Position const from, Position const to);

    // returns true if `from` and `to` are inside the grid
    bool in_grid() const;

    // displays the Move
    std::string display() const;

    static Move from_string(std::string const& input);
};
