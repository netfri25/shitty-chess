#include "Move.hpp"
#include <iostream>

Position::Position(int8_t const row, int8_t const col) : row(row), col(col) { }

Position Position::from_string(std::string const& input) {
    char const col_c = input[0];
    char const row_c = input[1];

    // bounds checking
    if (col_c < 'a' || col_c > 'h' || row_c < '1' || row_c > '8') {
        return Position(0, 0); // returns an invalid Position
    }

    uint8_t const col = col_c - 'a' + 1; // starts from 1
    uint8_t const row = row_c - '1' + 1; // starts from 1
    return Position(row, col);
}

bool Position::in_grid() const {
    auto const between = [](uint8_t x) { return x >= 1 && x <= 8; };
    return between(this->row) && between(this->col);
}

std::string Position::display() const {
    char const col_c = this->col + 'a' - 1;
    char const row_c = this->row + '1' - 1;
    return { col_c, row_c };
}

bool Position::operator==(Position const& other) const{
    return this->row == other.row && this->col == other.col;
}

// returns the position as an index in the grid
size_t Position::as_index() const {
    return (this->row - 1) * 8 + (this->col - 1);
}

// the position from an index in the grid
Position Position::from_index(size_t const index) {
    uint8_t const row0 = index / 8;
    uint8_t const col0 = index % 8;
    return Position(row0 + 1, col0 + 1);
}

Position& Position::operator+=(Position const& other) {
    this->row += other.row;
    this->col += other.col;
    return *this;
}

Position Position::operator+(Position const& other) const {
    Position result = *this;
    result += other;
    return result;
}

Move::Move(Position const from, Position const to) : from(from), to(to) { }

bool Move::in_grid() const {
    return this->from.in_grid() && this->to.in_grid();
}

std::string Move::display() const {
    return this->from.display() + this->to.display();
}


Move Move::from_string(std::string const& input) {
    std::string const from_str = std::string{input[0], input[1]};
    std::string const to_str = std::string{input[2], input[3]};
    Position const from = Position::from_string(from_str);
    Position const to = Position::from_string(to_str);

    return Move(from, to);
}
