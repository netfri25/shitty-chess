/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project,
in order to read and write information from and to the Backend
*/

#include "Board.hpp"
#include "Pipe.h"
#include <iostream>
#include <string.h>
#include <thread>
#include <cstring>

using std::cout;
using std::endl;
using std::string;

int main() {
    srand(time_t(NULL));


    // declare some variables
    Pipe p;
    bool isConnect = p.connect();

    // connect to the pipe
    string ans;
    while (!isConnect) {
        cout << "cant connect to graphics" << endl;
        cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
        std::cin >> ans;

        if (ans == "0") {
            cout << "trying connect again.." << endl;
            // WHY TF WOULD YOU EVEN WANT TO SLEEP??? I ACTUALLY THOUGH THIS DIDN'T WORK
            // Sleep(5000);
            isConnect = p.connect();
        } else {
            p.close();
            return 0;
        }
    }


    Board board = {};
    p.sendMessageToGraphics(board.display().c_str()); // send the board string

    // get message from graphics
    string msgFromGraphics = p.getMessageFromGraphics();

    while (msgFromGraphics != "quit") {
        // return result to graphics
        Move move = Move::from_string(msgFromGraphics);
        ResponseCode code = board.play_move(move);
        p.sendMessageToGraphics(std::to_string((int) code).c_str());

        // get message from graphics
        msgFromGraphics = p.getMessageFromGraphics();
    }

    p.close();
}
